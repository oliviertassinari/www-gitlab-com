---
layout: markdown_page
title: Product Direction - Fulfillment
description: "The Fulfillment Team at GitLab focus create and support the enablement of our customers to purchase, upgrade, downgrade, and renew licenses and subscriptions."
canonical_path: "/direction/fulfillment/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Last reviewed: 2021-08-19

## Overview

The Fulfillment section is passionate about creating seamless commerce experiences for our customers. We traverse [sales segments](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation), checkout preferences, and hosting options focused on ensuring customers can easily purchase, activate, and manage their subscriptions. Our goal is to increase net ARR while improving overall GTM efficiency, doing so will help GitLab scale growth and create an exponential lift while other departments and sections leverage our platform and experiences.

Currently our section is divided across three groups. Purchase is responsible for our primary checkout experiences and any product-driven purchase automation, License is responsible for provisioning of both SaaS and Self-Managed subscriptions, and Utilization takes care of usage reporting and usage admin controls. We also collaborate frequently across all of GitLab to achieve our goals. Most commonly we work with Sales Ops, Commercial and Enterprise Sales, Enterprise Applications, Growth and the Data teams. 

If you have any feedback on our direction we'd love to hear from you. Feel free and raise an MR, open an issue, or email Justin Farris. 

## Principles

Across our stable counterparts we follow four key principles (listed below). These principles keep us focused on delivering the right results and aide in enabling us to collaborate better with our stakeholders. These principles are not absolute, the intent is for them to guide our decision making. 

**Conducting business with GitLab should be seamless**

When customers choose to purchase GitLab they've already discovered value in the product, they know what they want and they're ready to unlock additional value by accessing the features / services enabled by a transaction. We should strive to ensure our transacting experiences fade into the background. This creates a better customer experience (no one wants to labor over buying something, they just want to use the product they bought), and result in accelerated growth for GitLab. 

**Build a strong foundation so GitLab can scale and other teams can collaborate more effectively**

Fulfillment's systems are often the foundational layer for a lot of the commerce conducted within the organization. We interface directly with Zuora (our SSOT for all subscriptions and transactions), maintain the licensing system that provisions a purchase for customers, and are the source of data for many KPIs and trusted data models. These systems need to be reliable, scale with demand and provide a solid surface area for other teams to collaborate on top of. If we do this well teams at GitLab don't need our time and resources as they take a dependency on our systems. 

**Use data to make decisions and measure impact**

On the Fulfillment team we have many sensing mechanisms at our disposal: customers are a zoom call away, we sync with our counterparts across the business weekly, and our issue tracker is full of improvement suggestions raised by GitLab team members and members of the wider community. We're also beginning to improve how we use data as a sensing mechanism to set direction and prioritization. Understanding our funnel is paramount in building a seamless commerce experience for our customers, to do so the Fulfillment teams will ensure we've properly instrumented each point in our transaction funnels and leverage analysis of that data to inform our strategy and direction. 


**Iterate, especially when the impact of a change is sizeable**

Iteration is one of the most challenging values to follow, especially within Fulfillment. Often times our work needs to be bundled and aligned closely with external announcements or communication. Even if that is unavoidable the Fulfillment team strives to break work down as much as possible and always be searching for the smallest possible iteration that delivers value to our customers or the business. 

## Vision

Our vision is to build an exceptional commerce experience for our customers, an experience that "gets out of the way" of the product and enables GitLab to scale growth as we add more and more customers. Delivering on this vision will mature all interfaces where customers conduct business with GitLab, regardless of customer size or needs they'll be able to transact smoothly and begin using what they've purchased quickly and efficiently. By focusing on building an exceptional experience we'll also enable GitLab team members contributing to GTM activities. Sales can scale to spend more time on accounts with a [high LAM](https://about.gitlab.com/handbook/sales/sales-term-glossary/#landed-addressable-market-lam), while functions like Support and Finance will spend less time manually supporting customers and our field teams. 

To achieve this vision we'll focus on the following areas:

- Build a best-in-class webstore
- Drive more transactions to self-service
- Deliver competitive purchasing options in sales-assisted and webstore orders
- Enable channel partners and distributors to deliver experiences as good as our direct sales motions
- Make license and user management fully self-service

**Build a best-in-class webstore**

Many new logos land as small first-orders. A common buyer persona in the webstore is [Alex](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager) choosing to purchase GitLab for their team. Alex may work at a small startup poised to grow, or a larger enterprise with some influence to adopt a new DevOps tool. Either way that initial experience with purchasing GitLab often starts in the webstore as a self-service transaction. This customer isn't necessarily ready to talk with sales, and only wants to make a small investment to get started with GitLab. By building a simple and easy to use transaction experience we can get out of Alex's way and get them back to [adopting newly acquired features and onboarding](https://about.gitlab.com/direction/growth/#drive-feature-adoption-and-usage). One of the best ways to do this is reducing friction from the point a customer has decided to purchase to when they're back in app developing their applications. This is why we're focused first on [moving our purchase flows](https://gitlab.com/groups/gitlab-org/-/epics/1888) out of a separate website (customers.gitlab.com) and into the core product. This forms the foundation that enables us to start iterate from. Our goal is for many other teams to contribute to that purchase experience, namely our Growth team counterparts who might run experiments improving conversion. Furthermore, we also aim to provide optionality at checkout and make the purchase experience tie more closely to the entire GTM self-service funnel. As one example: Imagine being able to run promotions that offer discounts connected to a marketing campaign, making that experience seamless will help improve conversion and lead to a better overall experience transacting with GitLab. 

**Drive more self-service transactions**

The focus here is efficiency, enabling more and more transactions to be started and completed via the webstore will free up our GTM counterparts to spend more time on higher value activities. Naturally the most benefit we get here is from SMB accounts but the vision is to enable support across our entire customer base. Mid-Market and Enterprise customers may want to transact completely self-service OR add-on to their GitLab subscription with incremental spend (e.g. purchase additional CI minutes). The fastest and most efficient way to do this for both the customer and our field teams is to enable that capability digitally. To do this well everyone in the loop needs visibility into the transactions. Our field teams may be compensated from those transactions and the customer needs a clear understanding of their total spend on GitLab, even if some of those transactions originated via different "storefronts". 

A key aspect to ensuring success in this area is a strong partnership with our GTM teams, especially our [VP of Online Sales & Self Service](https://about.gitlab.com/job-families/sales/vice-president-online-sales-and-self-service/). We'll need to map out the correct customer journeys across sales-segements and enable customers to self-select as they progress through the purchase experience. 

**Deliver competitive purchasing options in sales-assisted and webstore orders**

While the majority of GitLab transactions occur with a credit card and pay up-front for anything they purchase, that's not necessarily the preferred method for a large part of the world. Outside of the US the preferred digital payment method is an e-wallet and many customers as they grow and scale have more complex payment and billing requirements. To meet global demand we need to support multiple payment types and options. This can include multiple payment types, support for complex billing motions automatically via the webstore (POs, ACH transactions, etc), and support for multiple currencies. 

**Enable channel partners and distributors to deliver experiences as good as our direct sales motions**

More and more GitLab customers begin their journey with GitLab via a partner. They may transact in a cloud provider's marketplace or purchase with a larger bundling of software via a distributor. Our goal is to ensure those customers and partners get as high quality of service as they would buying direct. This means extending our APIs to support "indirect" transactions and collaborating closely with our counterparts in Channel Ops, Finance, and Enterprise Apps to design solutions that extend our internal systems beyond their direct-sales use-cases. 

**Make license, consumption and user-management fully self service**

With GitLab 14.1 we launched [Cloud Licensing](https://docs.gitlab.com/ee/subscriptions/self_managed/#cloud-licensing) this provides a foundation for an improved licensing and provisioning experience for our customers. For years GitLab was behind in modernizing our licensing systems and architecture, now that we've caught up we need to continue to iterate to help customers scale and reduce friction in license management. There are two key areas to focus on in the future: 

- **Enterprise License Management** The larger the instance the more challenging license and seat management becomes. These customers often need multiple instances, move large amounts of users in and out of the application each month and have complicated configuration requirements. To that end we will begin to unpack these problems and deliver solutions that help customers with larger instances easily deploy licenses and manage provisioning over the entire enterprise. The goal is to ensure [Sidney](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator) and [Skyler](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#skyler---the-chief-information-security-officer) can easily deploy licenses to meet the evolving needs of their business. 

- **Usage and Reporting** The most common Fulfillment-related questions customers ask after they've transacted is related to usage and consumption. While the majority of GitLab transactions occur on an annual subscription model there is still incremental usage and spend customers need to manage each month or quarter. This comes in the form of add on users, and consumables (storage and CI minutes). Managing usage based spend can be a headache for any customer. Organizations of any size need predictability in costs so our goal is to deliver transparent and clear reporting of seat usage, consumption and any other metered usage/billing option GitLab offers. 

### Purchase Group

Vision to be added

### License Group

Vision to be added

### Utilization Group

Vision to be added

## Group Responsiblities

**Purchase**
The Purchase group is responsible for all self-service purchase experiences, supports sales-assisted purchasing, channel and distributor e-marketplaces, subscription management (reconciliation, auto-renewal, invoicing, payment collection, etc), and trials. The group's primary goal is increasing self-service purchase volume to 95.5%. 

- Creating SKUs
- Purchase flows in GitLab.com
- Purchase flows in Customer Portal
- Trial flow & CTAs (SaaS & SM)
- Auto-Renewal processes
- Quarterly Subscription Reconciliation processes
- Subscription management features in GitLab.com (invoices, subscription cards, credit cards)
- Emails/In-app notification related to subscription management

**License**
The License group is responsible for provisioning and managing licenses across self-managed and SaaS (including Cloud License activation/sync and provisioning of legacy licenses) and support tooling. 

- Provisioning/Deprovisioning of Trials
- Provisioning/Deprovisioning of SaaS & SM plans
- Provisioning/Deprovisioning of consumables (storage, CI minutes)
- Emails/In-app notification related to cloud license
- Admin tools (CustomersDot, LicenseDot, GitLab.com)
- Updating downstream sales and marketing systems (SFDC, Marketo)
- Modifying Sentry error logging

**Utilization**
The Utilization group is responsible for all usage reporting and management, usage admin controls, CI minute managenent and storage management. 

- Features related to visualization of consumables (storage, CI minutes)
- Billable users, max users calculations in SM & SaaS
- Emails/In-app notification related to consumption (users/CI/storage)

## Roadmap

We use this roadmap to track the initiatives the Fulfillment section is working on. Our roadmap is strictly prioritized and scheduled following our [Project management process](https://about.gitlab.com/handbook/engineering/development/fulfillment/#project-management-process). We aim to update this roadmap every month as a part of our milestone [planning process](https://about.gitlab.com/handbook/engineering/development/fulfillment/#planning). This roadmap is also manually mirrored to GitLab in our [epic roadmap view](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=WEEKS&timeframe_range_type=CURRENT_QUARTER&label_name%5B%5D=Fulfillment+FY22-Q4).

To request work to be added to the Fulfillment roadmap, please follow our [Intake request process](https://about.gitlab.com/handbook/engineering/development/fulfillment/#intake-request). Changes in priorities of this roadmap follow our [Prioritization process](https://about.gitlab.com/handbook/engineering/development/fulfillment/#prioritization). 

_Last updated: 2021-11-08._

* All dates shown are completion dates
* If something is in progress, the status should show "X%" complete otherwise the status should show "Backlog".
* The easiest way to edit this table is to use [this spreadsheet](https://docs.google.com/spreadsheets/d/17IfBrltEWM49z6__NxbyuYkqdIWRAdqIJ6_UHgOa0no/edit?usp=sharing) and simply copy and paste changes over. The markdown table will be auto-generated if you paste the table into a Issue or MR description field.
* This roadmap is also available within GitLab in this [epic roadmap view](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=WEEKS&timeframe_range_type=CURRENT_QUARTER&label_name%5B%5D=Fulfillment+FY22-Q4).
* This roadmap was last updated: 2021-12-13

| Priority | ID     | Dependecy  | Initiative                                                                                                                                      | Team               | Status  | Validation | Build      | Test       | Rollout    | Notes                                                       |
|----------|--------|------------|-------------------------------------------------------------------------------------------------------------------------------------------------|--------------------|---------|------------|------------|------------|------------|-------------------------------------------------------------|
| 1        | A      | -          | [Increase QSR, Auto Renewal, Cloud Licensing Adoption (SaaS, SM)](https://app.ally.io/objectives/1631390?tab=undefined&time_period_id=135093)   | Purchase, License  | TBD     | -          | -          | -          | -          | -                                                           |
|          | A1     | -          | [QSR Accuracy and UX (SaaS, SM)]()                                                                                                              | Purchase           | TBD     | WIP        | TBD        | TBD        | TBD        | -                                                           |
|          | A1-i   | -          | [QSR Accuracy of max seat count (SaaS, SM)](https://gitlab.com/groups/gitlab-org/-/epics/7258)                                                  | Purchase           | 50%     | WIP        | TBD        | TBD        | TBD        | -                                                           |
|          | A1-ii  | -          | [QSR UX at time of purchase (SaaS, SM)](https://gitlab.com/groups/gitlab-org/-/epics/7229)                                                      | Purchase           | 5%      | WIP        | TBD        | TBD        | TBD        | -                                                           |
|          | A1-iii | -          | [QSR UX at time of adding users (SaaS)](https://gitlab.com/groups/gitlab-org/-/epics/7230)                                                      | Purchase           | 5%      | WIP        | TBD        | TBD        | TBD        | -                                                           |
|          | A2     | A1         | [Offline QSR, Auto Renewal, Cloud Licensing (SM)]()                                                                                             | Purchase, License  | 60%     | -          | 2022-02-17 | TBD        | TBD        | -                                                           |
|          | A2-i   | -          | [Offline QSR (SM)]()                                                                                                                            | Purchase           | X%      | -          | -          | TBD        | TBD        | -                                                           |
|          | A2-ii  | -          | [Offline Auto Renewal (SM)]()                                                                                                                   | Purchase           | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A2-iii | -          | [Offline Cloud Licensing (SM)]()                                                                                                                | License            | 60%     | -          | 2022-02-17 | TBD        | TBD        | -                                                           |
|          | A2-iv  | -          | [Offline Strict Cloud Licensing (SM)](https://gitlab.com/groups/gitlab-org/-/epics/7220)                                                        | License            | 10%     | -          | 2022-02-17 | TBD        | TBD        | -                                                           |
|          | A3     | A1, A2     | [Self-Serve QSR, Auto Renewal, Cloud Licensing (SaaS, SM)]()                                                                                    | Purchase, License  | 10%     | -          | 2022-02-17 | TBD        | TBD        | -                                                           |
|          | A3-i   | -          | [Self-Serve QSR (SaaS, SM)](https://gitlab.com/groups/gitlab-org/-/epics/5560)                                                                  | Purchase           | 100%    | 2021-11-23 | 2021-11-02 | 2021-11-23 | 2021-12-01 | -                                                           |
|          | A3-ii  | -          | [Self-Serve Auto Renewal (SaaS, SM)]()                                                                                                          | Purchase           | 0       | TBD        | TBD        | TBD        | TBD        | Must be completed before 2022-08-01 (date of first renewal) |
|          | A3-iii | -          | [Self-Serve Cloud Licensing (SM)]()                                                                                                             | License            | 100%    | -          | 2021-08-01 | TBD        | TBD        | -                                                           |
|          | A3-iv  | A1, A2     | [Self-Serve Strict Cloud Licensing (SM)](https://gitlab.com/groups/gitlab-org/-/epics/7263)                                                     | License            | 10%     | -          | 2021-02-17 | TBD        | TBD        | -                                                           |
|          | A4     | -          | [Sales Assisted excluding PO/Channel QSR, Auto Renewal, Cloud Licensing (SaaS, SM)]()                                                           | Purchase, License  | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A4-i   | -          | [Sales Assisted excluding PO/Channel QSR  (SaaS, SM)](https://gitlab.com/groups/gitlab-org/-/epics/5560)                                        | Purchase           | 100%    | 2021-11-23 | 2021-11-02 | 2021-11-23 | 2021-12-01 | -                                                           |
|          | A4-ii  | -          | [Sales Assisted excluding PO/Channel Auto Renewal (SaaS, SM)]()                                                                                 | Purchase           | 0%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A4-iii | -          | [Sales Assisted excluding PO/Channel Cloud Licensing (SM)]()                                                                                    | License            | 100%    |            | 2021-08-01 | TBD        | TBD        | -                                                           |
|          | A4-iv  | A1, A2, A3 | [Sales Assisted excluding PO/Channel Strict Cloud Licensing (SM)]()                                                                             | License            | 0%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A5     | -          | [PO QSR, Auto Renewal, Cloud Licensing (SaaS, SM)]()                                                                                            | Purchase, License  | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A5-i   | -          | [PO QSR (SaaS, SM)]()                                                                                                                           | Purchase           | X%      | WIP        | TBD        | TBD        | TBD        | -                                                           |
|          | A5-ii  | -          | [PO Auto Renewal (SaaS, SM)]()                                                                                                                  | Purchase           | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A5-iii | -          | [PO Cloud Licensing (SM)]()                                                                                                                     | License            | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A5-iv  | A1, A2, A3 | [PO Strict Cloud Licensing (SM)]()                                                                                                              | License            | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A6     | -          | [Channel QSR, Auto Renewal, Cloud Licensing (SaaS, SM)]()                                                                                       | Purchase, License  | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A6-i   | -          | [Channel QSR (SaaS, SM)]()                                                                                                                      | Purchase           | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A6-ii  | -          | [Channel Auto Renewal (SaaS, SM)]()                                                                                                             | Purchase           | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A6-iii | -          | [Channel Cloud Licensing (SM)]()                                                                                                                | License            | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A6-iv  | A1, A2, A3 | [Channel Strict SM Cloud Licensing (SM)]()                                                                                                      | License            | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A7     | -          | [Encourage eligible customers on License File to switch to Cloud Licensing before renewal](https://gitlab.com/gitlab-com/Product/-/issues/3527) | Marketing          | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A7-i   | -          | [Encourage customers to upgrade to 14.1+](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/3857)                                     | Marketing          | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A7-ii  | -          | [Improve dashboards for Cloud Licensing eligibility](https://gitlab.com/gitlab-com/Product/-/issues/3488)                                       | Program Management | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A7-iii | -          | [Investigate removing premium support for customers on older versions]()                                                                        | Support            | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A7-iv  | -          | [Get feedback from eligible customers who chose to not use Cloud Licensing](https://gitlab.com/gitlab-org/ux-research/-/issues/1709)            | Program Management | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A8     | A1         | [QSR rollover and edge cases ]()                                                                                                                | Purchase           | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
|          | A9     | A1         | [QSR - SM/SAAS - Skipped reconciliations due to data integrity issues](https://gitlab.com/groups/gitlab-org/-/epics/7259)                       | Purchase           | X%      | TBD        | TBD        | TBD        | TBD        | -                                                           |
| 2        | B      | -          | [Migrate CustomersDot infrastructure from Azure to GCP](https://gitlab.com/groups/gitlab-org/-/epics/7094)                                      | InfraDev           | 68%     | -          | 2021-12-13 | 2022-01-17 | 2022-01-31 | -                                                           |
| 3        | C      | -          | [Storage Project](https://gitlab.com/groups/gitlab-org/-/epics/3472)                                                                            | Utilization        | X%      | -          | 2022-05-01 | TBD        | TBD        | -                                                           |
|          | C1     | -          | [Move storage purchase flows to GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/3472)                                                  | Utilization        | X%      | -          | 2021-12-01 | TBD        | TBD        | -                                                           |
|          | C2     | C1         | [Storage sub 1](https://gitlab.com/groups/gitlab-org/-/epics/5789)                                                                              | Utilization        | Backlog | -          | 2022-01-01 | TBD        | TBD        | -                                                           |
|          | C3     | C1         | [Storage sub 2](https://gitlab.com/groups/gitlab-org/-/epics/7090)                                                                              | Utilization        | Backlog | -          | 2022-02-01 | TBD        | TBD        | -                                                           |
|          | C4     | C1         | [Storage sub 3](https://gitlab.com/groups/gitlab-org/-/epics/7091)                                                                              | Utilization        | Backlog | -          | 2022-03-01 | TBD        | TBD        | -                                                           |
| 4        | D      | -          | [Enable User Caps for SaaS Groups](https://gitlab.com/groups/gitlab-org/-/epics/5803)                                                           | Utilization        | X%      | -          | 2022-01-01 | TBD        | TBD        | -                                                           |
| 5        | E      | -          | [Add Analytics (Snowplow) to Webstore](https://gitlab.com/groups/gitlab-org/-/epics/6527)                                                       | Utilization        | X%      | -          | 2021-12-01 | TBD        | TBD        | -                                                           |
| 6        | F      | -          | [Enable Ramp deals](https://gitlab.com/groups/gitlab-org/-/epics/5679)                                                                          | License            | X%      | -          | WIP        | TBD        | TBD        | -                                                           |
| 7        | F1     | -          | [Migrate to Zuora Orders Harmonization](https://gitlab.com/groups/gitlab-org/-/epics/6641)                                                      | License            | X%      | 2021-12-01 | 2021-12-01 | TBD        | TBD        | -                                                           |
|          | F2     | F1         | [Ramps MVC using manual provisioning](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/3680)                                         | License            | Backlog | 2022-02-01 | 2022-02-01 | TBD        | TBD        | -                                                           |
|          | F3     | F1, F2     | [Ramps MVC using automated provisioning](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/3680#timeline-mvc)                         | License            | Backlog | TBD        | TBD        | TBD        | TBD        | -                                                           |
| 8        | G      | -          | [Promo to Increase FO Web Conversion](https://gitlab.com/groups/gitlab-org/-/epics/7029)                                                        | Purchase           | Backlog | TBD        | 2022-07-01 | TBD        | TBD        | -                                                           |
| 9        | H      | -          | [Channel integration with eDisty (Arrow) Marketplace](https://gitlab.com/groups/gitlab-org/-/epics/6873)                                        | Purchase, License  | Backlog | 2022-03-01 | 2022-05-01 | TBD        | TBD        | -                                                           |
{: .table-responsive style="margin-bottom: 25px; white-space: nowrap; display: block;"}

## OKRs

We follow the [OKR (Objective and Key Results)](https://about.gitlab.com/company/okrs/) framework to set and track goals on a quarterly basis. The Fulfillment section OKRs are set across the enture [Quad](https://about.gitlab.com/handbook/product/product-processes/#pm-em-ux-and-set-quad-dris):

* [FY22-Q4 Quad OKR's](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/320)

## Performance Indicators
Below is a list of performance indicators we track amongst the Fulfillment team. Note: some metrics are non-public so they may not be visible in the handbook. 

**[Percentage of transactions through self-service purchasing](https://about.gitlab.com/handbook/product/performance-indicators/#percentage-of-transactions-through-self-service-purchasing)**

**[Percentage of SMB transactions through self-service purchasing](https://app.periscopedata.com/app/gitlab/779889/WIP:-Percentage-of-SMB-transactions-through-self-service-purchasing?widget=10313311&udv=0)**

**[CI Minutes Transactions and Revenue](https://app.periscopedata.com/app/gitlab/744221/CI-minutes-Pricing-Initiative)**

**[Storage Consumption](https://app.periscopedata.com/app/gitlab/768903/GitLab-Storage)**
