---
layout: handbook-page-toc
title: "Handbook-First Approach to Interactive Learning"
description: "All elearning courses tied to the Field Certification Program will utilize the Handbook as the Single Source of Truth (SSOT)"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Handbook First Learning Model

All elearning courses at GitLab will utilize the Handbook as the Single Source of Truth (SSOT). 

![gitlab-learning-content-diagram](/handbook/people-group/learning-and-development/interactive-learning/gitlab-learning-content-diagram.png)

All our available learning material can be found on [GitLab Learn](https://about.gitlab.com/learn/).

Team members should follow [these important steps](/handbook/marketing/inbound-marketing/digital-experience/website/#updating-content-on-gitlab-learn) to update GitLab Learn every time a new course or certification is created.

## Handbook-First Interactive Learning Examples

### Field Enablement 
The GitLab Field Enablement team uses a handbook-first approach to learning in their [Field Certification Program](/handbook/sales/training/field-certification/) 

For example, in order to learn more about a particular topic tied to the Field Certification, learners will be able to refer to the Handbook within the elearning course instead of having to open a new browser tab and manually navigating to the Handbook.

The Field Enablement team will utilize the Articulate 360 Storyline Suite as the main course authoring tool. In Storyline, a [Handbook-first approach](/company/culture/all-remote/handbook-first-documentation/#gitlab-knowledge-assessment-handbook-first-documentation) is achieved by adding a web object on a slide so learners can access the Handbook on that particular topic.

### Learning and Development

The L&D team hosts [Mini and Extended Challenges](https://about.gitlab.com/handbook/people-group/learning-and-development/#gitlab-mini-and-extended-challenges) to engage team members in bite sized learning about a specific topic. Participants in the challenge collaborate on issues and are driven to the handbook to review learning content.


## Interactivity in E-learning

Interactivity such as games, drag-and-drop, matching, and other interactive components in a published elearning course will not be accessible via the Handbook; however, all information conveyed to learners through the elearning course will have an associated Handbook page as needed. This will make sure that despite the interactivity of the elearning course, learners will have access to all topics covered in the Field Certification program via the Handbook.

## Course Development Workflow

Instructional designers will use the ADDIE instructional systems design (ISD) framework to design and develop Field Certification courses using Articulate 360. The workflow of course development will be as follows:
1. Create learning objectives
1. Design storyboard and get SME buy in
1. Outline course Handbook page 
1. Develop elearning
1. Conduct a course Alpha
1. Incorporate feedback
1. Conduct a course Beta (as needed)
1. Publish course in LXP
1. Announce course launch
1. Enroll learners (as needed)

## Documenting learning content in the handbook

Our goal is that each course will have an associated Handbook page including learning objectives, static learning content, quizzes, and applicable links.

Consider the following when documenting course content in the handbook:

1. Outline clear learning objectives
1. Make content discoverable. For example, leadership content should not live in the Learning and Development handbook, but instead in the [Leadership handbook](/handbook/leadership/)
1. Embed relevant YouTube videos in the handbook
1. Link to knowledge assessments
1. Cross reference any additional handbook content with links

## Using highlighted links in the handbook

Google Chrome browsers can use the [copy link to highlight](https://support.google.com/chrome/answer/10256233?hl=en&co=GENIE.Platform%3DDesktop) feature. This will highlight a specified section of the handbook when clicked.

Note that this will only work with Google Chrome, so be mindful of the audience you are reaching before using this kind of link.

This kind of link is powerful to use when directing learners to the handbook as it **clearly marks where they should start and stop reading**.

Here is an example of how to create these links: 

1. Highlight the section of the page you want to appear highlighted
1. Right click and choose `Copy link to highlight`
1. Direct users to generated link for highlighted section

![gitlab-learning-content-diagram](/handbook/source/handbook/people-group/learning-and-development/interactive-learning/copy_link_to_highlight.jpeg)

It's important to note that high traffic or frequently updated handbook pages might result in broken links if the first or last word in a highlighted section is edited.

## Building handbook first learning content in the EdCast LXP

Content in the EdCast LXP at GitLab will adopt this handbook-first approach to learning. You can read more best practices about building content in the LXP in our [GitLab Learn admin docs](/handbook/people-group/learning-and-development/gitlab-learn/admin). Examples of ways that the handbook is utilized for learning in the LXP include:

- SmartCards linking to specific handbook pages
- Pathways leading learners through related handbook pages to train on a specific skill or content area
- iFrames displaying content from the handbook directly in the LXP
- Assessments and learning checks referring back to content in the handbook to check for understanding

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTfoGoGjXNlVNJs86aBniKhimx2-23wlDw40UJrGB6170GAcQeceB3naGrTgwqgsx-IKsybgLQV61Lf/embed?start=true&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

### Handbook First Video

The following best practices should be followed when using video to create handbook first learning material.

1. Upload recorded video to the GitLab Unfiltered channel
1. Embed videos into the handbook on the appropriate content page
1. Ensure that embedded videos have a sub-header on the page. This will be an essential piece when linking to the handbook using SmartCards or iframes. Here is an [example of this layout](/company/culture/all-remote/mental-health/#do-not-celebrate-working-long-hours).
1. Add videos to SmartCards by linking to these sub headers
1. Build iframes to display video by linking to these sub headers

#### Private Video

The following best practices should be followed when building learning content that includes private or internal only video.

1. Professional services shares recordings of live learning sessions directly with customers involved in each specific training via a Zoom link and recording password. These videos are not published on YouTube or to the handbook. Future iterations will include using EdCast to store and share these recordings using private Groups.

#### Why not use the GitLab Unfiltered API in EdCast?

Not all video on the GitLab Unfiltered channel is handbook first. In order to be sure that video in EdCast aligns with the handbook first best practices outlined above, we've elected to turn off the GitLab Unfiltered API in EdCast. 

### Learning Video Best Practices

In addition to making video learning content handbook first, consider these best practices when recording video training or delivering live training:

1. Turn on [Zoom closed captionining and live transcription](https://support.zoom.us/hc/en-us/articles/207279736-Closed-captioning-and-live-transcription)
1. Whenever possible, upload public videos to YouTube and use the YouTube link to share video on EdCast. This allows users to enable YouTube `closed caption` feature
1. Upload `.vtt` files with video content added to Rise courses using Articulate360 to include closed captions within the course.
1. The L&D team collaborates with Marketing Ops to use the tool Smartling to hard code closed captions to video that is not uploaded to YouTube. Video files should be shared with the Markteting Ops team who can support collaboration with Smartling. Expect anywhere between 1 day to 1 week turnaround on the video transcription based on the video length.

### Handbook first course example

Below is an outlined example of how the field enablement team created a handbook first learning course for the EdCast LXP using Articulate 360.

1. **Introduction** The course begins with an introduction, explaining to the learner what the objectives are and how to navigate the course content
1. **Video** Videos are embedded in the handbook and linked from the LXP
1. **iFrames** iFrames display content directly from the GitLab handbook. Before each iframe, a short description sets the expectation of the learner, time needed to read the handbook section, and a direct link to the handbook to open in a new tab
1. **Intellectual property** Intellectual property is linked directly to the course. Relevant handbook pages are linked and displayed in iframes when appropriate 
1. **Interactive Learning** Interactive learning provides opportunities for quick learning checks and does not introduce new material

### Building courses with Articulate360

Articulate360 is a powerful course creation tool. Below are a set of best practices and tips for creating handbook first courses using Articulate360:

1. Our team has a GitLab branded template that can be used for course creation. The template has GitLab colored theme, buttons, and font. You can use this by clicking on the `Course Template` > `Duplicate` > and then rename using your course title
1. Use [iframes](/handbook/people-group/learning-and-development/interactive-learning/#using-iframes-to-display-the-GitLab-handbook) to display sections of the GitLab handbook in your course

#### Using iframes to display the GitLab handbook

This [training video](https://youtu.be/wlLKZz3vSAk
) provides an example of how iframes can be used to display sections of the handbook in a Rise 360 course.

<iframe width="560" height="315" src="https://www.youtube.com/embed/wlLKZz3vSAk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In the team Articulate - Rise 360 license, there is a block template for iframes. You can access this template in the the `block templates` section and add your iframe code directly.

![iframe-embed-template](/handbook/people-group/learning-and-development/interactive-learning/iframe-block.png)


##### iframe tag examples

Consider copying and pasting the iframe tag below to apply the strategies outlined in the training. GitLab team members can access [this Google doc resource](https://docs.google.com/document/d/1HvikZayptzqT8kFFB_EPdiXBl04iYsnRzwFKGKgLVxI/edit) for ease of access to iframe tags that can be copied and edited when building courses using Articulate360 tools.

1. `<iframe src="link-to-your-page" title="title for iframe metadata" scrolling="no" height="XX"></iframe>`


### Examples of what is not considered handbook first

Avoiding these examples can help ensure that the content you're producing follows a handbook first approach

| Example | Suggested handbook first improvement |
| ----- | ----- |
| Introducing new content within a course or Pathway | Add new content to an existing handbook page, or open a new page to document |
| Directly uploading a video to the LXP | Upload the video first to GitLab Unfiltered, emebed on the correct handbook page, and either display in an Articulate course via iframe or link from a SmartCard in the LXP |

### Identified challenges with handbook first learning

- iframes can break if the page or subsection of the linked handbook is updated
- Updates to courses that are saved as SCORM files must be updated in the course creation tool and then reuploaded to the EdCast platform
- When iframes are not used, the user needs to navigate to text and video content in the handbook on a new internet tab
- New handbook/docs pages may need to be created in cases where videos should be included in a learning path





