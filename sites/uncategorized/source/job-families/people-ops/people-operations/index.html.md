---
layout: job_family_page
title: "People Operations"
---

The People Operations Specialist plays a critical role in influencing and supporting the people strategy within the GitLab - collaboratively working to develop, optimize and implement people processes and programs while overseeing the day-to-day responsibilities associated with people operations.

## People Operations Specialist

## Levels

### People Operations Specialist (Intermediate)

The People Operations Specialist (Intermediate) reports to the Manager, People Operations.

#### Job Grade

The People Operations Specialist (Intermediate) is a [Grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Document, implement and work to improve upon processes, procedures and programs relating to the team member lifecycle - conducting regular reviews around optimization and scalability.
- Ensure the delivery of critical people operations processes e.g. relocations (global mobility services), unemployment claims, offboarding and job information changes including compensation, promotions and transfers.
- Take full ownership of team member documentation including contracts, amendments and all other items pertinent to the team member lifecycle.
- Accountable for HRIS data including team member records and information changes such as promotions, terminations and transfers.  Ensuring the integrity and accuracy of all people data both in the HRIS and ancillary systems.
- Ensure that the People Experience Associate team has a full understanding of operational processes and policies providing guidance, support and training when necessary.
- Serve as a primary point of escalation and internal team member support for more complex cases, providing policy guidance and interpretation as needed (People Connect).
- In collaboration with various cross-functional partners, build and drive timely implementation of projects relating to the various points within the team member lifecycle coordinating workstreams and associated communication mechanisms.
- Support the People Business Partner (PBP) team i.e. administration of various surveys, conduct exit interviews (IC) and provide useful data to guide decision making.
- Provide functional and technical support surrounding the employee experience platform i.e. general configuration, trouble-shooting issues, managing settings and recommending process improvements.
- Collaborate with the Employment Solutions Partner to support relocation requests, fostering a close relationship with co-employers to ensure quality delivery of mobility services.


#### Requirements

- Bachelor's Degree with related majors e.g. Human Resources (HR), Industrial Psychology or Business Management **or** three years of experience in a generalist role ideally within a growth-stage tech company with exposure to either an all-remote or hybrid environment (minimum).
- Understanding of the entire team member lifecycle, with the ability to recommend process-oriented and/or operational solutions to improve the team member experience.
- Experience with Greenhouse, Workday, GitLab and Culture Amp (ideal).
- Ability to work within a distributed team and as an individual contributor in a fast-paced, changing environment.
- Strong verbal and written communications with the ability to effectively communicate at multiple levels within the company.
- Strong problem solving and organizational skills - able to manage multiple priorities in a dynamic and occasionally ambiguous environment.
- Able to use discretion and handle highly sensitive information.
- Excellent interpersonal skills and ability to establish strong relationships at all levels and across functions.

### Senior People Operations Specialist

The Senior people Operations Specialist reports to the Manager, People Operations.

#### Job Grade

The Senior People Operations Specialist is a [Grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Serve as a Subject Matter Expert (SME) on all people operations processes, policies and tools.
- Translate legal and regulatory requirements into system or process requirements in collaboration with the People Compliance Partner.
- Develop and maintain a system to ensure that people policies are consistently updated and shared.
- Monitor and leverage metrics and trends to determine process improvement opportunities ultimately working to enhance the team member experience.
- Collaborate with Employment Solutions and various other stakeholders such as third party employers to ensure that regional employment contracts, policies and practices are aligned to the unique requirements of each location i.e. data retention, legislative, regulatory and taxation.
- Support the implementation and testing of people tools and technologies including but not limited to the HRIS.
- Serve as a Subject Matter Expert (SME) for various modules within the HRIS.
- Accountable for knowledge transfer within the People Operations team in close collaboration with various stakeholders e.g. Total Rewards, - Learning and Development, Stock Administration etc.
- Work closely with the People Operations Manager to review and maintain the project roadmap in alignment with the needs of the company - identifying and escalating blockers, recommending changes and additions to scope and prioritizing action items.
- Guide the People Experience Team working to ensure that the team member journey is consistent and aligned to the values of the company, serving as a point of escalation as needed.
- Program Management i.e. responsible for the delivery of key programs such as the 360 Feedback Cycle, Engagement Surveys and Performance Assessments.
- Conduct exit interviews for Managers, Directors and E-Group Members identifying and reporting on trends.

#### Requirements

- Bachelor's Degree with related majors e.g. Human Resources (HR), Industrial Psychology or Business Management **or** five years of experience in a generalist role ideally within a growth-stage tech company with exposure to either an all-remote or hybrid environment (minimum).
- Understanding of the entire team member lifecycle, with the ability to recommend process-oriented and/or operational solutions to improve the team member experience.
- Experience with Greenhouse, Workday and Culture Amp (ideal).
- Ability to work within a distributed team and as an individual contributor in a fast-paced, changing environment.
- Strong verbal and written communications with the ability to effectively communicate at multiple levels within the company.
- Strong problem solving and organizational skills - able to manage multiple priorities in a dynamic and occasionally ambiguous environment.
- Able to use discretion and handle highly sensitive information.
- Excellent interpersonal skills and ability to establish strong relationships at all levels and across functions.

## Manager, People Operations

The Manager, People Operations reports to the Senior Manager, People Operations.

#### Job Grade

The Manager, People Operations is a [Grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Onboard, mentor, and grow the careers of all team members.
- Coach and mentor PeopleOps team to effectively address team member queries in line with our values.
- Work with Director, People Operations to shape a strategy that aligns and moves GitLab towards continued growth, innovation and improvement.
- Manage and  suggest improvements for program management including rollout, communication, reporting and metrics of Organizational health, Engagment Survey, DIB related surveys, 360 feedback cycle and Performance/ Potential Matrix Assessment. 
- Provide training and support to the Specialist group to address team member and leadership queries effectively and timely.
- Oversee the co-employer relationships and act as the main point of contact for the People Group.
- Seek and review potential blockers of various People Operations processes and ensuring improvement on daily tasks and suggest automation where needed.
- Review and improve Slack answers and continue to train and mentor the team to update information in the handbook, for easy access to all.
- Manage and maintain an SLA to track response times for email queries across timezones.
- Continue to drive automation for easy access of employment confirmation letters, automatic invitations to Contribute, confirmation of business insurance, etc.
- Continuous managing and improvement of both administration and people experience during onboarding.
- Partnering closely with Managers at GitLab to gain feedback from various teams about onboarding, offboarding and transition issues.
- Review and report on People Operations and Onboarding metrics, including [OSAT](/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat) and [Onboarding Task Completion](/handbook/people-group/people-group-metrics/#onboarding-task-completion--x-tbd).
- Continuous improvement of offboarding for both voluntary and involuntary terms.
- Along with the People Business Partners, create a Terminations playbook to ensure the team can remain async and scale.
- Work closely with payroll, SecOps, and ITOps to continue to improve the administration of offboarding the time it takes to complete.
- Mananging issue traiging and label review/ adding and reviewing contributions to the efficiency of our issue tracker.
- Drive continued automation and efficiency to enhance the employee experience and maintain our efficiency value.
- Announcing changes and improvements in the #whats-happening-at-gitlab Slack channel
- Leading positive employee experience throughout their lifecycle with GitLab.
- Implementation and monthly report on trends from Stay Interviews.
- Quarterly report on trends from exit interview data.
- Manage vendor renewals and agreements.
- Partner with Finance Business Partner for budgeting purposes.

#### Requirements

- Ability to use GitLab
- The ability to work autonomously and to drive your own performance & development would be important in this role
- Prior extensive experience in a People Operations role
- Willingness to work odd hours when needed (for example, to call an embassy in a different continent)
- Excellent written and verbal communication skills
- Exceptional customer service skills
- team player who can jump in and support the team on a variety of topics and tasks
- Enthusiasm for and broad experience with software tools
- Proven experience quickly learning new software tools
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Proven organizational skills with high attention to detail and the ability to prioritize
- You share our values, and work in accordance with those values
- The ability to work in a fast-paced environment with attention to detail is essential
- High sense of urgency and accuracy
- Experience at a growth-stage tech company

### Performance Indicators

- 12 month team member retention
- 12 month voluntary team member turnover
- Onboarding Satisfaction Survey > 4.5[](/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat)
- Onboarding task completion < X (TBD)

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
- Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a 45 minute interview with our People Operations Manager
- After that, candidates will be invited to schedule a 30 minute interview with members of the People Operations and People Partner teams
- After that, candidates will be invited to interview with the Director of People Operations
- Finally, our CPO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

## Senior Manager, People Operations

The Senior Manager, People Operations reports to Director, People Operations. 

#### Job Grade

The Senior Manager, People Operations is a [Grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Onboard, mentor, and grow the careers of all team members
- Provide coaching to improve performance of team members and drive accountability
- Work with Director, People Operations to shape a strategy that aligns and moves GitLab towards continued growth, innovation and improvement
- Provide training and support to the Specialist group to address team member and leadership queries effectively and timely
- Oversee the co-employer relationships and act as the main point of contact for the People Group
- Seek and review potential blockers of various People Operations processes and ensuring improvement on daily tasks and suggest automation where needed
- Improve Slack answers and continue to train and mentor the team to update information in the handbook, for easy access to all
- Implement an SLA to track response times for email queries across timezones
- Continue to drive automation for easy access of employment confirmation letters, automatic invitations to Contribute, confirmation of business insurance, etc
- Implementation of data retention strategies and partnering with People Leads to ensure this is implemented across all team member data consistently
- Continuous Improvement of both administration and people experience during onboarding.
- Partnering closely with Managers at GitLab to gain feedback from various teams about onboarding, offboarding and transition issues
- Review and report on People Operations and Onboarding metrics, including [OSAT](/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat) and [Onboarding Task Completion](/handbook/people-group/people-group-metrics/#onboarding-task-completion--x-tbd)
- Continuous improvement of offboarding for both voluntary and involuntary terms
- Along with the People Business Partners, create a Terminations playbook to ensure the team can remain async and scale
- Work closely with payroll, SecOps, and ITOps to continue to improve the administration of offboarding the time it takes to complete
- Mananging issue traiging and label review/ adding and reviewing contributions to the efficiency of our issue tracker. Provide strategic guidance and coaching on more difficult discussions and collaboration within issues. 

#### Requirements

- Ability to use GitLab
- The ability to work autonomously and to drive your own performance & development would be important in this role
- Prior extensive experience in a People Operations role
- Willingness to work odd hours when needed (for example, to call an embassy in a different continent)
- Excellent written and verbal communication skills
- Exceptional customer service skills
- Team player who can jump in and support the team on a variety of topics and tasks
- Enthusiasm for and broad experience with software tools
- Proven experience quickly learning new software tools
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Proven organizational skills with high attention to detail and the ability to prioritize
- You share our values, and work in accordance with those values
- The ability to work in a fast-paced environment with attention to detail is essential
- High sense of urgency and accuracy
- Experience at a growth-stage tech company

### Performance Indicators

- [12 month team member retention](/handbook/people-group/people-success-performance-indicators/#team-member-retention-rolling-12-months)
- [12 month voluntary team member turnover](/handbook/people-group/people-success-performance-indicators/#team-member-voluntary-retention-rolling-12-months)
- Onboarding Satisfaction Survey > 4.5[](/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat)
- Onboarding task completion < X (TBD)

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 45 minute interview with our People Operations Manager
* After that, candidates will be invited to schedule a 30 minute interview with members of the People Operations and People Partner teams
* After that, candidates will be invited to interview with the Director of People Operations
* Finally, our CPO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

## Director, Global People Operations

The Director of Global People Operations reports to the [Senior Director, People Success](/job-families/people-ops/people-leadership/).

#### Job Grade

The Director, Global People Operations is a [Grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Lead and manage a growing People Operations team responsible for managing the employee experience throughout the employee lifecycle.
- Help shape a global People Operations strategy that aligns and moves the business towards continued growth, innovation and improvement.
- Provide HR support and consultation to the business; answering employee and manager questions about HR programs, policies, and other HR-related items.
- Provide HR support and leadership with respect to day-to-day HR life cycle activities of client groups, including performance management issues, investigations, and reorganizations.
- Understand workforce needs as the company scales managing costs while staying competitive with salary, benefits, perks, leaves, etc.
- Work with the executive team to come up with a competitive compensation strategy.
- Assess and apply market data, ensuring alignment with the company’s total compensation philosophy.
- Oversee performance, development, and the compensation review process.
- Drive a progressive, proactive, positive culture, and increase levels of engagement, enablement, and retention.
- Coach, influence, and provide guidance to business partners to figure out optimal organizational design, development, and performance management plans.
- Assist with the maintenance and accuracy of HR data, including processing SAP data, such as Employee Action Forms.
- Provide resolution to employee and organizational issues in a proactive and sensitive manner.
- Assess/identify HR strategy, policy, or process improvements.
- Ensure People Operations strategies and processes remain aligned with the company’s talent management and workforce plans to enhance employee engagement and sustain business growth.
- Collaborate with Chief People Officer, People Operations, and organizational leaders in fostering a culture of empowerment and performance; establish the employee lifecycle journey and succession plans.
- Maintain in-depth knowledge of local, global, and federal employment laws; maintain and store records judiciously and securely.

#### Requirements

- 8+ years of progressive experience in HR roles with a demonstrable track record of building and optimizing processes, systems, and structures.
- Requires a Bachelor’s degree preferably in Human Resources, Organizational Development, Organizational Leadership, or related field.
- 5+ years hands-on experience with compensation & benefits.
- Labor Relations experience required, preferably in multiple countries.
- Working knowledge of regulatory and legal requirements related to total rewards and systems.
- 3-5 years of experience leading people and cross-functional organizations.
- Demonstrable ability to own, execute and deliver on short- and long-term projects.
- Strategic and innovative thinker; able to prioritize and use sound judgment and decision-making.
- Executive presence with excellent written and oral communication skills.
- Business insight and high EQ to successfully collaborate with executives and business partners at all levels.
- Successful completion of a background check -- see the [GitLab Code of Business Conduct and Ethics](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d).
- You share our [values](/handbook/values/), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)
- Ability to use GitLab

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 45 minute interview with our Senior Manager, People Operations
* After that, candidates will be invited to schedule a 30 minute interview with members of the People Business Partner Team 
* Next, candidates will be invited to schedule a 30 minute interview with members of the Legal team
* After that, candidates will be invited to interview with the Senior Director, People Success
* Finally, our CPO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

## Career Ladder

The next step in the People Operations job family is to move to the [People Leadership Job Family](/job-families/people-ops/people-leadership/).
