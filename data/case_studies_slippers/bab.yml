title: Bendigo and Adelaide Bank
file_name: bab
canonical_path: /customers/bab/
cover_image: /images/blogimages/bab_cover_image.jpg
cover_title: |
  Learn how GitLab is accelerating DevOps at Bendigo and Adelaide Bank
cover_description: |
  Discover how the move from GitHub to GitLab advanced its cloud journey, increased efficiency, and reduced operating costs
twitter_image: /images/blogimages/bab_cover_image.jpg
twitter_text: Learn how @bendigobank is accelerating DevOps transformation with GitLab
customer_logo: /images/case_study_logos/babl_logo.png
customer_logo_css_class: brand-logo-tall
customer_industry: Financial Services 
customer_location: Bendigo, Australia
customer_solution: GitLab Ultimate SaaS
customer_employees: |
  7,000
customer_overview: |
  Bendigo and Adelaide Bank is Australia’s better big bank, helping more than 2 million customers achieve their financial goals.
customer_challenge: |
  Bendigo and Adelaide Bank sought a solution to accelerate cloud computing, simplify a complex toolchain, and reduce operational costs.
key_benefits: >-

  
    Simplified toolchain complexity 

  
    Supports rapid AWS cloud migration

  
    Facilitated multi cloud deployments

  
    A single solution for SCM, CI/CD, and security

  
    Decreased operational costs
customer_stats:
  - stat: 4 weeks
    label: Rapid migration in less than 4 weeks (1,500 projects, over 30 organisations, 500 users, and 50GB of data)
  - stat: 3 to 1  
    label: Tool simplification from 3 to 1 (GitLab replaces GitHub, Jenkins and Checkmarx)
  - stat: 32 in 30 
    label: Completes 32 apps in 30 days shift (migration of 32 workloads to AWS in 30 days) 
customer_study_content:
  - title: the customer
    subtitle: One of Australia’s most trusted banks
    content: >-
    
  
       As Australia's better big bank, [Bendigo and Adelaide Bank](https://www.bendigoadelaide.com.au/) is community-focused and dedicated to supporting its customers by ensuring fairness and equity in its pricing. Committed to its customers and communities, Bendigo and Adelaide Bank has delivered high-quality customer service for over 160 years, maintaining its values of teamwork, integrity, performance, engagement, leadership, and passion.  In 2019, the bank announced a multi-year transformation strategy focused on reducing complexity and investing in digital transformation. 

  - title: the challenge
    subtitle:  Existing solution had high operational costs and complex tooling
    content: >-
    

       The team at Bendigo and Adelaide Bank experienced a few challenges with their GitHub on-premise solution. They needed significant operational resources and heavy engineering to maintain their GitHub instance. Compounding the challenge was their reliance on other tools for CI/CD and security, and the team struggled with a complex toolchain. The lack of a single source of truth meant team members were unable to have full visibility in the software development lifecycle and tracking metrics became difficult.

       Understanding that continual operational support for an on-premise solution was unsustainable, Bendigo and Adelaide Bank sought a SaaS solution that would offer a robust platform. In addition, the bank was focused on a solution that would align with its strategic objectives of reducing complexity, support agility and promote continuous innovation. 


  - title: the solution
    subtitle:  A single solution accelerates business transformation
    content: >-

       The team used GitHub only for source code management and relied on other tools to complement its software development practices. In search of a solution, the team hoped to find a tool that would decrease toolchain complexity and create a centralised location to find information. The team initially assessed GitHub as a SaaS solution, but they didn’t see all the features they needed to meet their goals. 

       Continuing their search, the team was impressed with GitLab, believing it to be a comprehensive solution to increase operational efficiency, create a single source of truth, and simplify tooling. The team turned to GitLab to manage runners, support Kubernetes, and use security features, such as SAST, container security, and secrets management. 

       “By reducing the number of tools, we have lower maintenance costs, since we don’t need to spend money for on-prem instances and physical servers. We were able to shift to SaaS easily with GitLab. We’ve also avoided the cost of upgrading legacy systems and patching.Using GitLab, we’ve removed complexity from our tech stack, and now we’re more agile. Overall, everyone likes GitLab. It improves our time to market.” said Caio Trevisan, Head of DevOps Enablement. 
      
        The team uses GitLab to implement elevated permissions to control access to projects to require code reviews before merging. “GitLab makes privilege and access management easy. We also now have visibility and observability by using infrastructure as code,” shared Caio. Using CI pipelines, it’s easier for the team to analyse an application and have end-to-end visibility when doing deep analyses. Infrastructure as code has also helped the team have better reverting capabilities and governance.
       
        
  - title: the results
    subtitle:  Increased cloud computing and operational efficiency
    content: >-

        In migrating to GitLab, the team moved 1,500 projects, over 30 organizations, 500 users, and 50GB of data in four weeks. The team is now rapidly progressing towards meeting the corporate goal to move 50% of its applications to the cloud in three to five years. Accelerating business transformation is an important part in managing costs and maintaining sustainable growth. 


        With GitLab, the team has embraced cloud technology and has automated manual processes. “GitLab helps us with multi cloud deployments. We can deploy runners in any infrastructure, and we’re currently using them to deploy to AWS and GCP. Deploying to the cloud has been simple, and in the year we’ve been using GitLab, we’re in a good position to meet our goal of moving to the cloud,” explained Caio. The team’s CI runners are deployed everywhere, and team members appreciate that they always scale. 


        Since using GitLab, the team has experienced increased communication. “With merge requests and code review capabilities, we’re able to collaborate more. Everyone knows that GitLab is our central tool, so we have a single source of truth where everyone can discuss projects,” said Caio. With tooling simplification, the team has not only streamlined its workflow but also improved productivity. The organisation has seen an added benefit of attracting new talent to Bendigo and Adelaide Bank, since software professionals want to use market leading technology to innovate. The company is well-known for its ambitious growth and transformation strategy, and by simplifying technology, the Bank has become more innovative and agile in responding to their customers’ needs.


        The team has observed that GitLab has helped with onboarding new hires. By only having to learn one tool, with useful templates, new hires have been able to push code on their second day. The team has created an internal training service called “DevOps Academy,” which onboards new developers to the simplified tech stack in one week. As an open source project, DevOps Academy uses GitLab to teach team members their development workflow.


        The team is looking forward to embracing GitLab features more deeply by moving away from Jenkins for CD and using GitLab to identify and track metrics. Reflecting on the move to GitLab, Caio shared, “We've been getting good feedback about GitLab from other teams. The team is really jumping into it and learning how to use it. Our workflow is more streamlined and efficient, and we’re accelerating business transformation.” In migrating to GitLab, the team moved 1,500 projects, over 30 organisations, 500 users, and 50GB of data in less than four weeks. GitLab represents the bank's commitment to achieving its strategic objectives by reducing complexity, investing in new capabilities, accelerating its cloud journey to shape its vision to be Australia’s bank of choice.
       

        

    
  
        ## Learn more about GitLab solutions
    
  
        [Security with GitLab](/solutions/dev-sec-ops/)
    
  
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: We now have an always-innovating solution that aligns with our goal of digital transformation.
    attribution: Caio Trevisan
    attribution_title: Head of DevOps Enablement, Bendigo and Adelaide Bank








