title: Parts Unlimited
canonical_path: /customers/parts_unlimited/
cover_image: /images/blogimages/partsunlimited.jpg
cover_title: |
  How Parts Unlimited quadrupled stakeholder value using GitLab
cover_description: |
  Using a fictitious company from The Unicorn Project, we show you how using GitLab CI and SCM can digitally transform an enterprise from brick and mortar to a successful online business.
twitter_image: /images/blogimages/partsunlimited.jpg
twitter_text: With GitLab CI and SCM, Parts Unlimited digitally transformed from brick and mortar to a successful online business.
customer_logo: /images/case_study_logos/unicorn-project.png
customer_logo_css_class: brand-logo-wide
customer_industry: Retail
customer_location: US
customer_solution: GitLab Ultimate
customer_employees: |
      7,000 employees
customer_overview: |
  Working to get its online market up and running, Parts Unlimited found success with GitLab.
customer_challenge: |
  With disparate toolchains, a lack of collaboration, and zero source control, developers struggled to deliver products on-demand.

key_benefits: >-

   
    End-to-end visibility

   
    Increased operational efficiency

   
    Delivering better products faster

   
    Easy cloud integration

   
    Open source collaboration

customer_stats:
  - stat: |
        $460,000
    label: In annual savings
  - stat: |
        26,000
    label: Pounds of obsolete equipment removed and recycled
  - stat: 4x
    label: Stakeholder value

customer_study_content:
  - title: the customer
    subtitle: A respected automotive distributor
    content: >-
    
   
        _Parts Unlimited is a fictitious company in the novels [The Unicorn Project](https://itrevolution.com/the-unicorn-project) and [The Phoenix Project](https://itrevolution.com/book/the-phoenix-project/) by Gene Kim. The challenges and metrics are pulled from these books to create a "before" business scenario for Parts Unlimited. GitLab has created this case study to illustrate how it could help Parts Unlimited or any similar company with DevOps workflow, cloud integration, and open source collaboration. We hope you enjoy imagining all that is possible with GitLab._
    
   
        For over 100 years, Parts Unlimited has been one of the largest automotive parts suppliers in the United States. With nearly a thousand stores across the country, Parts Unlimited has millions of loyal customers.

  - title: the challenge
    subtitle: Disparate workflows, zero documentation, and unhappy developers
    content: >-
    
   
        Parts Unlimited was falling behind their previously stellar sales records and decreasing shareholder value. The company lacked an online presence and recently started a push towards becoming a digital company. With an intense internal drive for transformation, it lost sight of ways to improve customer value.
    
   
        For software development to be successful, creating it needed to be an interactive endeavor, but Parts Unlimited lacked the capabilities to allow developers to collaborate. There was zero source code management, network credentials were needed at every step, and license keys were necessary for all applications. On top of that, the development team still used Gantt chart printouts -- essentially developing software without using software.
    
   
        There was no formal documentation process in place for any of the existing legacy tools. “We didn’t have a standard dev environment that developers could use. It took months for new developers to do builds on their laptops and be fully productive. Even our build server was woefully under-documented,” according to Randy Keyes, Dev Manager. Developer downtime was at an all-time high because they were unable to contribute to code checks.
    
   
        Parts Unlimited didn’t have mandated software tools in place, so new hires had to discover for themselves the various access points and the credentials they needed to utilize them. “If we all want our developers to be productive, they need to be able to perform builds on Day One,” said Maxine Chambers, Developer Lead, Architect, Parts Unlimited. “Ideally, they should be writing code in a production-like environment so they can get fast feedback on whether the code they write works with the system as a whole.”
    
   
        Without collaboration and constant feedback from a centralized build, integration, and test system, developers didn’t have control over what happens to their work once it's merged. Build failures happened constantly, but because there wasn’t a single source of truth, and finding the failure took time.
    
   
        Developers were overworked and unhappy with the inability to see the outcome of their efforts. Team members spent endless hours trying to deliver capabilities to the market. “I’ve opened up hundreds of tickets, trying to get things done. I’ve followed those tickets around, seeing where they went. Many of them went to Ops, some went to QA...It was just a giant circle of tickets,” Chambers said.
    
   
        Ideally, developers should be able to focus their energy on product development, rather than on the bugs and breaks. The MTTD and MTTR were long because of the constant context switching. Implementing a significant feature took between 20 to 40 weeks. QA costs were soaring because of the lack of automated testing.
    
   
        “Here’s what we needed: We needed an automated way to create environments and perform code builds,” According to Kurt Reznick, QA Manager, Parts Unlimited. “We needed some way to automate those tests and some automated way to get those builds deployed into production. We needed builds so that developers can actually do their work.” Parts Unlimited needed a platform that would help increase operational efficiency in order to deliver better products at a faster rate.

  - title: the solution
    subtitle: Stability in a singular SCM and CI/CD platform
    content: >-
    
   
        After testing out Jenkins, BitBucket, CircleCI, and other platforms, Parts Unlimited adopted GitLab. Other tools were valuable individually, but none could provide a single application for the entire DevOps lifecycle. Parts Unlimited’s needs spanned SCM, CI/CD, and all workflow process improvements they could acquire and GitLab offered the most value.
    
   
        [GitLab SCM](/stages-devops-lifecycle/source-code-management/) shifted the way the development team was able to work. Developers can utilize tools through a singular access point within the GitLab platform. There is no longer a need for license keys or several different logins, because of the built-in security and compliance. Software is deployed anywhere, which relieves developers localization constraints.
    
   
        Every team member uses a common build environment, supported by a continuous integration system. Developers can run their own code in production-like environments and are able to deliver value independently. [Small, quick iterations](/blog/2020/02/04/power-of-iteration/) allows teams to document who makes changes to code and where the iterations happen. Automated tests have replaced manual testing, freeing up QA teams to work on production level projects.

  - title: the results
    subtitle: Exceeding expectation in sales, software deliveries, and customer success
    content: >-
    
   
        Two years into its digital transformation, Parts Unlimited finally saw success after adopting GitLab. Automation has vastly improved the speed and level of innovation. [Collaboration now exists](/blog/2019/12/23/six-key-practices-that-improve-communication/), not only within the development team, but also interdepartmentally. Customer records are accessible, so employees can scan QR codes to fulfill orders. This is a game changer for customers, but also for employees who once struggled to access systems in order to carry out customer requests.
    
   
        Automated services allow developers to spend their time working on the product, not patching bugs. Throughput has increased and the technology team has doubled since its inception two year prior. Every business unit within the company is actively looking for more engineers, and with the ability to work from anywhere, engineers are being hired remotely.
    
   
        Cross-team promotions capabilities drive people to the mobile app, e-commerce site, and physical stores, creating burgeoning sales records. “In the last sixty days, in-store sales for our pilot stores are up almost seven percent,” according to Maggie Lee, Senior Director of Retail Program Management. “To put this into perspective, the nonpilot stores have had flat or negative same-store sales. This performance is extremely noteworthy and shows that better customer service enables more sales, which has always been a Parts Unlimited core value.”
    
   
        With a single DevOps platform in use, the company has saved an incredible amount of money, including $163,000 in email servers, $109,000 in helpdesk costs, and $188,000 in HR systems, equal to approximately $460,000 in annual savings. The stakeholder value of the company is four times higher than previously valued. Parts Unlimited is getting recognized by the Wall Street Journal as a thriving marketplace in an era of digital disruption.
customer_study_quotes:
  - blockquote: ...We’ve booked over $29 million in revenue today alone. We blew away last year’s sales record by a mile!
    attribution: Maggie Lee
    attribution_title: Senior Director of Retail Program Management, Parts Unlimited

